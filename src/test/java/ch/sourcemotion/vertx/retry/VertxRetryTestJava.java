package ch.sourcemotion.vertx.retry;

import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(VertxExtension.class)
class VertxRetryTestJava {

    private static final String ADDR = "test-addr";
    private static final String TEST_VALUE = "test-value";
    private static final VertxRetryOptions DEFAULT_TEST_OPTIONS = new VertxRetryOptions(5, 100);
    private static final VertxRetryOptions OPTIONS_WITHOUT_HANDLED_FAILURES = new VertxRetryOptions(5, 100,
            Collections.emptyList());

    @Test
    void usual_successful(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, DEFAULT_TEST_OPTIONS);

        vertx.eventBus().consumer(ADDR, msg -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
            msg.reply(null);
        });

        vertxRetry.request(ADDR, TEST_VALUE, context.succeeding(msg -> checkpoint.flag()));
    }

    @Test
    void usual_successful_promise(Vertx vertx, VertxTestContext context) {
        VertxRetry vertxRetry = VertxRetry.create(vertx, DEFAULT_TEST_OPTIONS);
        Checkpoint checkpoint = context.checkpoint();

        vertx.eventBus().consumer(ADDR, msg -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
            msg.reply(null);
        });

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE);
        context.assertComplete(promise.future()).setHandler(msg -> checkpoint.flag());
    }

    @Test
    void no_handler(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        vertxRetry.request(ADDR, TEST_VALUE, context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                final VertxRetryException e = (VertxRetryException) throwable;

                assertEquals(options.getRetries() - 1, e.getRetryCases().size());
                e.getRetryCases().forEach(failureCase -> assertEquals(VertxFailureCase.NO_HANDLERS, failureCase));
                assertEquals(VertxFailureCase.NO_HANDLERS, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void no_handler_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE);
        promise.future().setHandler(context.failing(throwable -> {
            assertTrue(throwable instanceof VertxRetryException);
            VertxRetryException e = (VertxRetryException) throwable;

            assertEquals(options.getRetries() - 1, e.getRetryCases().size());
            e.getRetryCases().forEach(failureCase -> assertEquals(VertxFailureCase.NO_HANDLERS, failureCase));
            assertEquals(VertxFailureCase.NO_HANDLERS, e.getFinalCase());
            checkpoint.flag();
        }));
    }

    @Test
    void handler_registered_later(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        vertxRetry.request(ADDR, TEST_VALUE, context.succeeding(msg -> checkpoint.flag()));

        vertx.setTimer(options.getRetryPause() + 10, timerId -> {
            vertx.eventBus().consumer(ADDR, msg -> {
                context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
                msg.reply(null);
            });
        });
    }

    @Test
    void handler_registered_later_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE);

        vertx.setTimer(options.getRetryPause() + 10, timerId -> {
            vertx.eventBus().consumer(ADDR, msg -> {
                context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
                msg.reply(null);
            });
        });

        context.assertComplete(promise.future()).setHandler(msg -> checkpoint.flag());
    }


    @Test
    void no_handler_not_handled(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, OPTIONS_WITHOUT_HANDLED_FAILURES);

        vertxRetry.request(ADDR, TEST_VALUE, context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                final VertxRetryException e = (VertxRetryException) throwable;

                assertTrue(e.getRetryCases().isEmpty());
                assertEquals(VertxFailureCase.NO_HANDLERS, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void no_handler_not_handled_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, OPTIONS_WITHOUT_HANDLED_FAILURES);

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE);
        promise.future().setHandler(context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertTrue(e.getRetryCases().isEmpty());
                assertEquals(VertxFailureCase.NO_HANDLERS, e.getFinalCase());
            });
            checkpoint.flag();
        }));
    }

    @Test
    void recipient_fails_each_event(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        vertx.eventBus().consumer(ADDR, msg -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
            msg.fail(0, "test-fail");
        });

        vertxRetry.request(ADDR, TEST_VALUE, context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertEquals(options.getRetries() - 1, e.getRetryCases().size());
                e.getRetryCases().forEach(failureCase -> assertEquals(VertxFailureCase.RECIPIENT_FAILURE, failureCase));
                assertEquals(VertxFailureCase.RECIPIENT_FAILURE, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void recipient_fails_each_event_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        vertx.eventBus().consumer(ADDR, msg -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
            msg.fail(0, "test-fail");
        });

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE);
        promise.future().setHandler(context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertEquals(options.getRetries() - 1, e.getRetryCases().size());
                e.getRetryCases().forEach(failureCase -> assertEquals(VertxFailureCase.RECIPIENT_FAILURE, failureCase));
                assertEquals(VertxFailureCase.RECIPIENT_FAILURE, e.getFinalCase());
            });
            checkpoint.flag();
        }));
    }

    @Test
    void recipient_fails_first_time(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, DEFAULT_TEST_OPTIONS);

        AtomicInteger failedCount = new AtomicInteger();

        vertx.eventBus().consumer(ADDR, msg -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
            if (failedCount.getAndIncrement() == 0) {
                msg.fail(0, "test-fail");
            } else {
                msg.reply(null);
            }
        });

        vertxRetry.request(ADDR, TEST_VALUE, context.succeeding());
        checkpoint.flag();
    }

    @Test
    void recipient_fails_first_time_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, DEFAULT_TEST_OPTIONS);

        AtomicInteger failedCount = new AtomicInteger();

        vertx.eventBus().consumer(ADDR, msg -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));

            if (failedCount.getAndIncrement() == 0) {
                msg.fail(0, "test-fail");
            } else {
                msg.reply(null);
            }
        });

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE, new DeliveryOptions());
        promise.future().setHandler(context.succeeding());
        checkpoint.flag();
    }

    @Test
    void recipient_fails_not_handled(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, OPTIONS_WITHOUT_HANDLED_FAILURES);

        vertx.eventBus().consumer(ADDR, msg -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
            msg.fail(0, "test-fail");
        });

        vertxRetry.request(ADDR, TEST_VALUE, context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertTrue(e.getRetryCases().isEmpty());
                assertEquals(VertxFailureCase.RECIPIENT_FAILURE, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void recipient_fails_not_handled_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, OPTIONS_WITHOUT_HANDLED_FAILURES);

        vertx.eventBus().consumer(ADDR, msg -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
            msg.fail(0, "test-fail");
        });

        vertxRetry.request(ADDR, TEST_VALUE, context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertTrue(e.getRetryCases().isEmpty());
                assertEquals(VertxFailureCase.RECIPIENT_FAILURE, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void timeout_each_event(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        vertx.eventBus().consumer(ADDR, msg -> context.verify(() -> assertEquals(TEST_VALUE, msg.body())));

        vertxRetry.request(ADDR, TEST_VALUE, new DeliveryOptions().setSendTimeout(100), context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertEquals(options.getRetries() - 1, e.getRetryCases().size());
                e.getRetryCases().forEach(failureCase -> assertEquals(VertxFailureCase.TIMEOUT, failureCase));
                assertEquals(VertxFailureCase.TIMEOUT, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void timeout_each_event_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        vertx.eventBus().consumer(ADDR, (Message<String> msg) -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
        });

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE, new DeliveryOptions().setSendTimeout(100));
        promise.future().setHandler(context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertEquals(options.getRetries() - 1, e.getRetryCases().size());
                e.getRetryCases().forEach(failureCase -> assertEquals(VertxFailureCase.TIMEOUT, failureCase));
                assertEquals(VertxFailureCase.TIMEOUT, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void timeout_first_event(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, DEFAULT_TEST_OPTIONS);

        AtomicInteger timeoutCount = new AtomicInteger();

        vertx.eventBus().consumer(ADDR, (Message<String> msg) -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));

            if (timeoutCount.getAndIncrement() > 0) {
                msg.reply(null);
            }
        });

        vertxRetry.request(ADDR, TEST_VALUE, new DeliveryOptions().setSendTimeout(100), context.succeeding(message -> checkpoint.flag()));
    }

    @Test
    void timeout_first_event_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, DEFAULT_TEST_OPTIONS);

        AtomicInteger timeoutCount = new AtomicInteger();

        vertx.eventBus().consumer(ADDR, (Message<String> msg) -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));

            if (timeoutCount.getAndIncrement() > 0) {
                msg.reply(null);
            }
        });

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE, new DeliveryOptions().setSendTimeout(100));
        context.assertComplete(promise.future()).setHandler(msg -> checkpoint.flag());
    }

    @Test
    void timeout_not_handled(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, OPTIONS_WITHOUT_HANDLED_FAILURES);

        vertx.eventBus().consumer(ADDR, (Message<String> msg) -> {
            context.verify(() -> assertEquals(TEST_VALUE, msg.body()));
        });

        vertxRetry.request(ADDR, TEST_VALUE, new DeliveryOptions().setSendTimeout(100), context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertTrue(e.getRetryCases().isEmpty());
                assertEquals(VertxFailureCase.TIMEOUT, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void timeout_not_handled_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, OPTIONS_WITHOUT_HANDLED_FAILURES);

        vertx.eventBus().consumer(ADDR, msg -> context.verify(() -> assertEquals(TEST_VALUE, msg.body())));

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, TEST_VALUE, new DeliveryOptions().setSendTimeout(100));
        promise.future().setHandler(context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertTrue(e.getRetryCases().isEmpty());
                assertEquals(VertxFailureCase.TIMEOUT, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void other_case(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        vertx.eventBus().consumer(ADDR, msg -> context.failNow(new Exception("Should not be called")));

        vertxRetry.request(ADDR, new ClassWithoutCodec(), context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertEquals(options.getRetries() - 1, e.getRetryCases().size());
                e.getRetryCases().forEach(failureCase -> assertEquals(VertxFailureCase.OTHER, failureCase));
                assertEquals(VertxFailureCase.OTHER, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void other_case_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetryOptions options = DEFAULT_TEST_OPTIONS;
        VertxRetry vertxRetry = VertxRetry.create(vertx, options);

        vertx.eventBus().consumer(ADDR, msg -> context.failNow(new Exception("Should not be called")));

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, new ClassWithoutCodec());
        promise.future().setHandler(context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertEquals(options.getRetries() - 1, e.getRetryCases().size());
                e.getRetryCases().forEach(failureCase -> assertEquals(VertxFailureCase.OTHER, failureCase));
                assertEquals(VertxFailureCase.OTHER, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void other_case_not_handled(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, OPTIONS_WITHOUT_HANDLED_FAILURES);

        vertx.eventBus().consumer(ADDR, msg -> context.failNow(new Exception("Should not be called")));

        vertxRetry.request(ADDR, new ClassWithoutCodec(), context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertTrue(e.getRetryCases().isEmpty());
                assertEquals(VertxFailureCase.OTHER, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    @Test
    void other_case_not_handled_promise(Vertx vertx, VertxTestContext context) {
        Checkpoint checkpoint = context.checkpoint();
        VertxRetry vertxRetry = VertxRetry.create(vertx, OPTIONS_WITHOUT_HANDLED_FAILURES);

        vertx.eventBus().consumer(ADDR, msg -> context.failNow(new Exception("Should not be called")));

        Promise<Message<Object>> promise = vertxRetry.request(ADDR, new ClassWithoutCodec());
        promise.future().setHandler(context.failing(throwable -> {
            context.verify(() -> {
                assertTrue(throwable instanceof VertxRetryException);
                VertxRetryException e = (VertxRetryException) throwable;

                assertTrue(e.getRetryCases().isEmpty());
                assertEquals(VertxFailureCase.OTHER, e.getFinalCase());
            });

            checkpoint.flag();
        }));
    }

    public static class ClassWithoutCodec {
    }
}
