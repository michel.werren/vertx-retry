package ch.sourcemotion.vertx.retry

import io.kotlintest.shouldBe
import io.vertx.core.eventbus.ReplyException
import io.vertx.core.eventbus.ReplyFailure
import org.junit.jupiter.api.Test

internal class VertxFailureCaseTest {
    @Test
    internal fun forVertxReplyFailureTypes() {
        VertxFailureCase.forException(ReplyException(ReplyFailure.RECIPIENT_FAILURE))
            .shouldBe(VertxFailureCase.RECIPIENT_FAILURE)
        VertxFailureCase.forException(ReplyException(ReplyFailure.NO_HANDLERS)).shouldBe(VertxFailureCase.NO_HANDLERS)
        VertxFailureCase.forException(ReplyException(ReplyFailure.TIMEOUT)).shouldBe(VertxFailureCase.TIMEOUT)
    }

    @Test
    internal fun forOtherFailureTypes() {
        VertxFailureCase.forException(Exception()).shouldBe(VertxFailureCase.OTHER)
        VertxFailureCase.forException(IllegalStateException()).shouldBe(VertxFailureCase.OTHER)
    }
}