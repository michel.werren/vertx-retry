package ch.sourcemotion.vertx.retry

import io.kotlintest.matchers.collections.shouldBeEmpty
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.shouldBe
import io.vertx.core.Vertx
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.concurrent.atomic.AtomicInteger

@ExtendWith(VertxExtension::class)
class VertxRetryTest {

    companion object {
        private const val ADDR = "test-addr"
        private const val TEST_VALUE = "test-value"
    }

    /**
     * Positive test, when anything should work as expected.
     */
    @Test
    internal fun usual_successful(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(2)
        val vertxRetry = VertxRetry.create(vertx)
        testAsync(context, checkpoint) {
            vertx.eventBus().consumer<String>(ADDR) {
                checkpoint.flag()
                context.verify {
                    it.body().shouldBe(TEST_VALUE)
                }
                it.reply(null)
            }

            vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE)
        }
    }

    /**
     * If no handler is registered on the event bus for an address.
     */
    @Test
    internal fun no_handler(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val options = VertxRetryOptions().apply {
            retryOnCases = listOf(VertxFailureCase.NO_HANDLERS)
            retries = 5
            retryPause = 100
        }
        val vertxRetry = VertxRetry.create(vertx, options)
        testAsync(context, checkpoint) {
            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE)
                }
            }.let {
                it.retryCases.shouldHaveSize(options.retries - 1)
                it.retryCases.contains(VertxFailureCase.NO_HANDLERS)
                it.finalCase.shouldBe(VertxFailureCase.NO_HANDLERS)
            }
        }
    }

    /**
     * If no handler is registered at the moment the first event will approach. But later during retry, the handler becomes
     * registered and called.
     */
    @Test
    internal fun handler_registered_later(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(2)
        val options = VertxRetryOptions()
        val vertxRetry = VertxRetry.create(vertx, options.apply {
            retryOnCases = listOf(VertxFailureCase.NO_HANDLERS)
            retries = 5
            retryPause = 100
        })
        testAsync(context, checkpoint) {
            vertx.setTimer(options.retryPause + 10) {
                vertx.eventBus().consumer<String>(ADDR) {
                    context.verify {
                        it.body().shouldBe(TEST_VALUE)
                    }
                    it.reply(null)
                    checkpoint.flag()
                }
            }

            vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE)
        }
    }

    /**
     * If no handler is registered, but Vert.x retry is not configured to handle [VertxFailureCase.NO_HANDLERS].
     * The caller must fail fast.
     */
    @Test
    internal fun no_handler_not_handled(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val vertxRetry = VertxRetry.create(vertx, VertxRetryOptions().apply {
            retryOnCases = emptyList()
            retries = 5
            retryPause = 100
        })
        testAsync(context, checkpoint) {
            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE)
                }
            }.let {
                it.retryCases.shouldBeEmpty()
                it.finalCase.shouldBe(VertxFailureCase.NO_HANDLERS)
            }
        }
    }

    /**
     * If the the recipient fails on each event.
     */
    @Test
    internal fun recipient_fails_each_event(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(6)
        val options = VertxRetryOptions().apply {
            retryOnCases = listOf(VertxFailureCase.RECIPIENT_FAILURE)
            retries = 5
            retryPause = 100
        }
        val vertxRetry = VertxRetry.create(vertx, options)

        testAsync(context, checkpoint) {
            vertx.eventBus().consumer<String>(ADDR) {
                checkpoint.flag()
                context.verify {
                    it.body().shouldBe(TEST_VALUE)
                }
                it.fail(0, "test-fail")
            }

            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE)
                }
            }.let {
                it.retryCases.shouldHaveSize(options.retries - 1)
                it.retryCases.contains(VertxFailureCase.RECIPIENT_FAILURE)
                it.finalCase.shouldBe(VertxFailureCase.RECIPIENT_FAILURE)
            }
        }
    }

    /**
     * If the recipient fails on the first event, but later he succeeds.
     */
    @Test
    internal fun recipient_fails_first_time(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(3)
        val failedCount = AtomicInteger(0)
        val vertxRetry = VertxRetry.create(vertx, VertxRetryOptions().apply {
            retryOnCases = listOf(VertxFailureCase.RECIPIENT_FAILURE)
            retries = 2
            retryPause = 100
        })
        testAsync(context, checkpoint) {
            vertx.eventBus().consumer<String>(ADDR) {
                checkpoint.flag()
                context.verify {
                    it.body().shouldBe(TEST_VALUE)
                }
                if (failedCount.getAndIncrement() == 0) {
                    it.fail(0, "test-fail")
                } else {
                    it.reply(null)
                }
            }

            vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE)
        }
    }

    /**
     * If recipient fails, but Vert.x retry is not configured to handle [VertxFailureCase.RECIPIENT_FAILURE].
     * The caller must fail fast.
     */
    @Test
    internal fun recipient_fails_not_handled(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(2)
        val vertxRetry = VertxRetry.create(vertx, VertxRetryOptions().apply {
            retryOnCases = emptyList()
            retries = 5
            retryPause = 100
        })
        testAsync(context, checkpoint) {
            vertx.eventBus().consumer<String>(ADDR) {
                checkpoint.flag()
                context.verify {
                    it.body().shouldBe(TEST_VALUE)
                }
                it.fail(0, "test-fail")
            }

            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE)
                }
            }.let {
                it.retryCases.shouldBeEmpty()
                it.finalCase.shouldBe(VertxFailureCase.RECIPIENT_FAILURE)
            }
        }
    }

    /**
     * If the consumer runs into callers timeout on each event.
     */
    @Test
    internal fun timeout_each_event(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(6)
        val options = VertxRetryOptions().apply {
            retryOnCases = listOf(VertxFailureCase.TIMEOUT)
            retries = 5
            retryPause = 100
        }
        val vertxRetry = VertxRetry.create(vertx, options)

        testAsync(context, checkpoint) {
            vertx.eventBus().consumer<String>(ADDR) {
                context.verify { it.body().shouldBe(TEST_VALUE) }
                checkpoint.flag()
            }

            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE, DeliveryOptions().setSendTimeout(100))
                }
            }.let {
                it.retryCases.shouldHaveSize(options.retries - 1)
                it.retryCases.contains(VertxFailureCase.TIMEOUT)
                it.finalCase.shouldBe(VertxFailureCase.TIMEOUT)
            }
        }
    }

    /**
     * If the consumer runs into callers timeout on first event.
     */
    @Test
    internal fun timeout_first_event(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(3)
        val timeoutCount = AtomicInteger(0)
        val vertxRetry = VertxRetry.create(vertx, VertxRetryOptions().apply {
            retryOnCases = listOf(VertxFailureCase.TIMEOUT)
            retries = 5
            retryPause = 100
        })
        testAsync(context, checkpoint) {
            vertx.eventBus().consumer<String>(ADDR) {
                checkpoint.flag()
                context.verify {
                    it.body().shouldBe(TEST_VALUE)
                }
                if (timeoutCount.getAndIncrement() > 0) {
                    it.reply(null)
                }
            }

            vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE, DeliveryOptions().setSendTimeout(100))
        }
    }

    /**
     * If consumer runs into caller timeout, but Vert.x retry is not configured to handle [VertxFailureCase.TIMEOUT].
     * The caller must fail fast.
     */
    @Test
    internal fun timeout_not_handled(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(2)
        val vertxRetry = VertxRetry.create(vertx, VertxRetryOptions().apply {
            retryOnCases = emptyList()
            retries = 5
            retryPause = 100
        })
        testAsync(context, checkpoint) {
            vertx.eventBus().consumer<String>(ADDR) {
                checkpoint.flag()
                context.verify {
                    it.body().shouldBe(TEST_VALUE)
                }
            }

            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR, TEST_VALUE, DeliveryOptions().setSendTimeout(100))
                }
            }.let {
                it.retryCases.shouldBeEmpty()
                it.finalCase.shouldBe(VertxFailureCase.TIMEOUT)
            }
        }
    }

    /**
     * If runs into "other", may not event bus related exception (in this test unknown codec).
     */
    @Test
    internal fun other_case(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val options = VertxRetryOptions().apply {
            retryOnCases = listOf(VertxFailureCase.OTHER)
            retries = 5
            retryPause = 100
        }
        val vertxRetry = VertxRetry.create(vertx, options)

        testAsync(context, checkpoint) {
            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR, Unit)
                }
            }.let {
                it.retryCases.shouldHaveSize(options.retries - 1)
                it.retryCases.contains(VertxFailureCase.OTHER)
                it.finalCase.shouldBe(VertxFailureCase.OTHER)
            }
        }
    }

    /**
     * If runs into "other", may not event bus related exception (in this test unknown codec). But [VertxFailureCase.OTHER] is not
     * configured. The caller must fail fast.
     */
    @Test
    internal fun other_case_not_handled(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        val vertxRetry = VertxRetry.create(vertx, VertxRetryOptions().apply {
            retryOnCases = emptyList()
            retries = 5
            retryPause = 100
        })
        testAsync(context, checkpoint) {
            assertThrows(VertxRetryException::class.java) {
                runBlocking(vertx.dispatcher()) {
                    vertxRetry.requestAwait<Unit>(ADDR, Unit)
                }
            }.let {
                it.retryCases.shouldBeEmpty()
                it.finalCase.shouldBe(VertxFailureCase.OTHER)
            }
        }
    }
}