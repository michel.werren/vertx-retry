package ch.sourcemotion.vertx.retry

class VertxRetryOptions @JvmOverloads constructor(
    /**
     * Number of retries they will be send to consumer until the failure will returned to caller.
     */
    var retries: Int = DEFAULT_RETRIES,

    /**
     * Pause between retries in the case of failure.
     */
    var retryPause: Long = DEFAULT_RETRY_PAUSE,

    /**
     * Failure cases they should be covered. In all cases they are missing in [retryOnCases] will fail fast.
     * Means the very first fail will returned to caller.
     */
    var retryOnCases: List<VertxFailureCase> = DEFAULT_RETRY_FAILURE_CASES,

    /**
     * When not [VertxRetryLogLevel.NONE], failures will be logged, even max. retries not reached.
     */
    var failureLogLevel: VertxRetryLogLevel = DEFAULT_LOG_LEVEL
) {
    companion object {
        private const val DEFAULT_RETRIES = 5
        private const val DEFAULT_RETRY_PAUSE = 1000L
        private val DEFAULT_RETRY_FAILURE_CASES = VertxFailureCase.values().toList()
        private val DEFAULT_LOG_LEVEL = VertxRetryLogLevel.NONE
    }
}