package ch.sourcemotion.vertx.retry.impl

import ch.sourcemotion.vertx.retry.*
import io.vertx.core.*
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.eventbus.Message
import io.vertx.core.logging.LoggerFactory
import io.vertx.kotlin.coroutines.awaitResult
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

internal class VertxRetryImpl(
    private val vertx: Vertx,
    private val retryOptions: VertxRetryOptions = VertxRetryOptions()
) : VertxRetry {

    companion object {
        private val LOG = LoggerFactory.getLogger(VertxRetry::class.java)
    }

    override fun <T> request(address: String, body: Any?): Promise<Message<T>> {
        return request(address, body, DeliveryOptions())
    }

    override fun <T> request(address: String, body: Any?, options: DeliveryOptions): Promise<Message<T>> {
        val promise = Promise.promise<Message<T>>()
        GlobalScope.launch(vertx.dispatcher()) {
            try {
                val result = requestAwait<T>(address, body, options)
                promise.complete(result)
            } catch (e: Exception) {
                promise.fail(e)
            }
        }
        return promise
    }

    override fun <T> request(
        address: String,
        body: Any?,
        handler: Handler<AsyncResult<Message<T>>>
    ) {
        request(address, body, DeliveryOptions(), handler)
    }

    override fun <T> request(
        address: String,
        body: Any?,
        options: DeliveryOptions,
        handler: Handler<AsyncResult<Message<T>>>
    ) {
        GlobalScope.launch(vertx.dispatcher()) {
            try {
                val result = requestAwait<T>(address, body, options)
                handler.handle(Future.succeededFuture(result))
            } catch (e: Exception) {
                handler.handle(Future.failedFuture(e))
            }
        }
    }

    override suspend fun <T> requestAwait(address: String, body: Any?, options: DeliveryOptions): Message<T> =
        requestAwaitRecursive(0, mutableListOf(), address, body, options)

    private suspend fun <T> requestAwaitRecursive(
        previousTries: Int,
        retryCases: MutableList<VertxFailureCase>,
        address: String,
        body: Any?,
        options: DeliveryOptions
    ): Message<T> {
        val thisTryNumber = previousTries + 1
        return try {
            return awaitResult { vertx.eventBus().request(address, body, options, it) }
        } catch (e: Exception) {
            logFailure(e, thisTryNumber, retryOptions.retries, address)

            val failureCase = VertxFailureCase.forException(e)

            // If the failure case should be handled and there are tries left, retry. Otherwise fail.
            if (retryOptions.retryOnCases.contains(failureCase) && thisTryNumber < retryOptions.retries) {
                delay(retryOptions.retryPause)
                retryCases.add(failureCase)
                requestAwaitRecursive(thisTryNumber, retryCases, address, body, options)
            } else {
                throw VertxRetryException.build(retryCases, failureCase)
            }
        }
    }

    private fun logFailure(e: Exception, tryCount: Int, retries: Int, address: String) {
        val failureCase = VertxFailureCase.forException(e)

        if (retryOptions.failureLogLevel != VertxRetryLogLevel.NONE) {
            val msg = "Try $tryCount of $retries did fail on address: $address because of $failureCase"
            when {
                retryOptions.failureLogLevel == VertxRetryLogLevel.TRACE && LOG.isTraceEnabled -> {
                    LOG.trace(msg, e)
                }
                retryOptions.failureLogLevel == VertxRetryLogLevel.DEBUG && LOG.isDebugEnabled -> {
                    LOG.debug(msg, e)
                }
                retryOptions.failureLogLevel == VertxRetryLogLevel.INFO && LOG.isInfoEnabled -> {
                    LOG.info(msg, e)
                }
                retryOptions.failureLogLevel == VertxRetryLogLevel.WARN && LOG.isWarnEnabled -> {
                    LOG.warn(msg, e)
                }
                retryOptions.failureLogLevel == VertxRetryLogLevel.ERROR -> {
                    LOG.error(msg, e)
                }
            }
        }
    }
}