package ch.sourcemotion.vertx.retry

import ch.sourcemotion.vertx.retry.impl.VertxRetryImpl
import io.vertx.core.AsyncResult
import io.vertx.core.Handler
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.eventbus.Message

/**
 * Entry point and main api of Vertx retry.
 */
interface VertxRetry {
    companion object {
        @JvmStatic
        fun create(vertx: Vertx): VertxRetry =
            VertxRetryImpl(vertx)

        @JvmStatic
        fun create(vertx: Vertx, options: VertxRetryOptions = VertxRetryOptions()): VertxRetry =
            VertxRetryImpl(vertx, options)
    }

    fun <T> request(
        address: String,
        body: Any? = null
    ): Promise<Message<T>>

    fun <T> request(
        address: String,
        body: Any? = null,
        options: DeliveryOptions = DeliveryOptions()
    ): Promise<Message<T>>

    fun <T> request(
        address: String,
        body: Any? = null,
        handler: Handler<AsyncResult<Message<T>>>
    )

    fun <T> request(
        address: String,
        body: Any? = null,
        options: DeliveryOptions = DeliveryOptions(),
        handler: Handler<AsyncResult<Message<T>>>
    )

    suspend fun <T> requestAwait(
        address: String,
        body: Any? = null,
        options: DeliveryOptions = DeliveryOptions()
    ): Message<T>
}